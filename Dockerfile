FROM openjdk:11-jre-slim

WORKDIR /minecraft

EXPOSE 25565

ENTRYPOINT ["/minecraft/startserver.sh"]
